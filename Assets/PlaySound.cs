﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour {
	public AudioSource[] sources;

	public void PlaySoundByID(int id) {
		sources[id].Play();
	}
}
