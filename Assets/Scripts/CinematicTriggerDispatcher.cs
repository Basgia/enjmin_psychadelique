﻿using UnityEngine;
using System.Collections;

public class CinematicTriggerDispatcher : MonoBehaviour {
	public Animator anim;

	public void SendTrigger(string trigger) {
		anim.SetTrigger(trigger);
	}
}
