﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class ColumnScript : MonoBehaviour {
	public Material colorShift;
	public Transform playerTransform;
	public float distance;
	public float shiftMaxAmount;

	public BloomOptimized bloom;

	public Animator anim;

	private Transform myTransform;

	void Awake() {
		myTransform = transform;
	}

	void Update() {
		float playerDistance = (playerTransform.position - myTransform.position).magnitude;
		colorShift.SetFloat("_NoiseMin", Mathf.Clamp01(playerDistance / distance));
		colorShift.SetFloat("_ShiftAmount", shiftMaxAmount - shiftMaxAmount * Mathf.Clamp01(playerDistance / distance));
		bloom.threshold = 0.055f + 2f * Mathf.Clamp01(playerDistance / distance);
	}

	void OnCollisionEnter(Collision col) {
		if(col.collider.tag == "Player"){
			anim.SetTrigger("close");
		}
	}
}
