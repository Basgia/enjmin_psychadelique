﻿using UnityEngine;
using System.Collections;

public class CutsceneLauncher : MonoBehaviour {
	public Animator anim;
	public int cutsceneID = 1;

	private static int lastCutsceneID = 0;

	void OnTriggerEnter(Collider col) {
		if(lastCutsceneID != cutsceneID - 1)
			return;
		
		if(col.tag == "Player") {
			anim.SetTrigger("cutscene" + cutsceneID);
			lastCutsceneID = cutsceneID;
		}
	}
}
