﻿using UnityEngine;
using System.Collections;

public class EnableOnTrigger : MonoBehaviour {
	public bool enableObject = false;
	public GameObject target;

	void OnTriggerEnter(Collider col) {
		if(col.tag == "Player")
			target.SetActive(enableObject);
	}
}
