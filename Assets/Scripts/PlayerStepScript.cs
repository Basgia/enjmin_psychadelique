﻿using UnityEngine;
using System.Collections;

public class PlayerStepScript : MonoBehaviour {
	public Transform soundVisionMaterialUser;

	private Transform myTransform;
	private Material m;

	void Start() {
		GetComponent<FirstPersonController>().soundVisionMaterial = soundVisionMaterialUser.GetComponent<MeshRenderer>().sharedMaterial;
	}
}