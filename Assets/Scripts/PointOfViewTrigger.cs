﻿using UnityEngine;
using System.Collections;

public class PointOfViewTrigger : MonoBehaviour {
	public Camera povCamera;

	void OnTriggerEnter(Collider col) {
		if(col.tag == "Player") {
			povCamera.enabled = true;
		}
	}

	void OnTriggerExit(Collider col) {
		if(col.tag == "Player") {
			povCamera.enabled = false;
		}
	}
}
