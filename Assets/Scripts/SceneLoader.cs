﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	public string sceneName = "02_Planet";

	public void LoadScene() {
		SceneManager.LoadScene(sceneName);
	}
}
