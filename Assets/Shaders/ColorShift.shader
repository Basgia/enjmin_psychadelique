﻿Shader "ColorShift"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ShiftAmount ("Shift amount", Range(-2, 2)) = 0
		_NoiseMin("Noise min impact", Range(0, 1)) = 0.5
		_NoiseMax("Noise max impact", Range(0, 2)) = 1.3
	}
	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			float rand(float3 co)
			{
			    return frac(sin(_Time.w * dot(co.xyz , float3(12.9898,78.233,45.5432) )) * 43758.5453);
			}

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _ShiftAmount;
			float _NoiseMin;
			float _NoiseMax;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// Displace red
				col.r = tex2D(_MainTex, i.uv + float2(_ShiftAmount / 100, 0));
				// Displace blue
				col.b = tex2D(_MainTex, i.uv + float2(0, _ShiftAmount / 100)).b;
				// Displace green
				col.g = tex2D(_MainTex, i.uv + float2(_ShiftAmount / 100, _ShiftAmount / 100)).g;
				
				col = col * lerp(_NoiseMin, _NoiseMax, rand(i.vertex.xyz));

				return col;
			}
			ENDCG
		}
	}
}
