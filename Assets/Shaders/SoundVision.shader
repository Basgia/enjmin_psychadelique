// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "SoundVision" {
 	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_MainTint("Main Color", Color) = (1, 1, 1, 1)

		_StepTex ("Step texture", 2D) = "black" {}
		_StepTint("Step Color", Color) = (1, 1, 1, 1)

		_SpeedFactor("Speed factor", Range(1, 200)) = 20
		_Width("Width", Range(0, 30)) = 5
	}

   SubShader {
      Tags { "RenderType" = "Opaque" 
				"Queue" = "Geometry" }

      Pass {
	      CGPROGRAM
	            #pragma vertex vert
	            #pragma fragment frag

	            #include "UnityCG.cginc"

	            const float pi = 3.14159;

	            struct appdata
	            {
	                float4 vertex : POSITION;
	                float2 uv : TEXCOORD0;
	            };

	            struct v2f
	            {
	                float2 uv : TEXCOORD0;
	                float4 wp : VARYING;
	                float4 vertex : SV_POSITION;
	            };

				v2f vert (appdata v)
	            {
	                v2f o;
	                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	                o.wp = mul(unity_ObjectToWorld, v.vertex);
	                o.uv = v.uv;
	                return o;
	            }

	            sampler2D _MainTex;
	            sampler2D _StepTex;
	            fixed4 _MainTint;
	            fixed4 _StepTint;
	            float _SpeedFactor;
	            float _Width;

	            // No arrays :'(
	            fixed4 _LastFootPos0;
	            fixed4 _LastFootPos1;
	            fixed4 _LastFootPos2;
	            fixed4 _LastFootPos3;
	            fixed4 _LastFootPos4;

	            float _LastFootTime0;
	            float _LastFootTime1;
	            float _LastFootTime2;
	            float _LastFootTime3;
	            float _LastFootTime4;

	            fixed4 frag (v2f i) : SV_Target
	            {
	                // sample the texture
	                fixed4 col = tex2D(_MainTex, i.uv);
	                fixed4 stepCol = tex2D(_StepTex, i.uv);

	                // get step/point distance 0
	                float dist0 = distance(i.wp, _LastFootPos0);
	                float deltTime0 = _Time.y * _SpeedFactor - _LastFootTime0 * _SpeedFactor;
	                float4 intensity0 = saturate((deltTime0 - dist0)) - saturate((deltTime0 - dist0 - _Width));

	                // get step/point distance 1
	                float dist1 = distance(i.wp, _LastFootPos1);
	                float deltTime1 = _Time.y * _SpeedFactor - _LastFootTime1 * _SpeedFactor;
	                float4 intensity1 = saturate((deltTime1 - dist1)) - saturate((deltTime1 - dist1 - _Width)); 

	                // get step/point distance 2
	                float dist2 = distance(i.wp, _LastFootPos2);
	                float deltTime2 = _Time.y * _SpeedFactor - _LastFootTime2 * _SpeedFactor;
	                float4 intensity2 = saturate((deltTime2 - dist2)) - saturate((deltTime2 - dist2 - _Width)); 

	                // get step/point distance 3
	                float dist3 = distance(i.wp, _LastFootPos3);
	                float deltTime3 = _Time.y * _SpeedFactor - _LastFootTime3 * _SpeedFactor;
	                float4 intensity3 = saturate((deltTime3 - dist3)) - saturate((deltTime3 - dist3 - _Width)); 

	                // get step/point distance 4
	                float dist4 = distance(i.wp, _LastFootPos4);
	                float deltTime4 = _Time.y * _SpeedFactor - _LastFootTime4 * _SpeedFactor;
	                float4 intensity4 = saturate((deltTime4 - dist4)) - saturate((deltTime4 - dist4 - _Width)); 

	                stepCol = ((stepCol * _StepTint * intensity0) + 
	                	(stepCol * _StepTint * intensity1) + 
	                	(stepCol * _StepTint * intensity2) + 
	                	(stepCol * _StepTint * intensity3) + 
	                	(stepCol * _StepTint * intensity4));

	                col = col * _MainTint;

	                if(stepCol.a > 0.5) {
						return col * (1 - stepCol.a) + stepCol;
	                } else {
	                	return col;
	                }
	            }
	   		ENDCG
	   		}
   		}
}