﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Stencils/Skybox" {
    Properties {
       _Cube ("RefSkybox", Cube) = "" { }
     }
    SubShader {
   		Stencil {
			Ref 1
			Comp equal
			Pass keep
			Fail keep
		}
    	Pass {
        	CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

         struct vertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
         };

         struct vertexOutput {
            float4 pos : SV_POSITION;
            float3 normalDir : TEXCOORD0;
            float3 viewDir : TEXCOORD1;
         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;
 
            float4x4 modelMatrix = unity_ObjectToWorld;
            float4x4 modelMatrixInverse = unity_WorldToObject; 
 
            output.viewDir = mul(modelMatrix, input.vertex).xyz 
               - _WorldSpaceCameraPos;
            output.normalDir = normalize(
               mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
         }

         uniform samplerCUBE _Cube;   
 
         float4 frag(vertexOutput input) : COLOR
         {
            float3 reflectedDir = 
               reflect(input.viewDir, normalize(input.normalDir));
            return texCUBE(_Cube, reflectedDir);
         }
            ENDCG
        }
    }
}
